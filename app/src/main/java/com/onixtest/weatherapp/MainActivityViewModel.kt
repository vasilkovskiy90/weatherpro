package com.onixtest.weatherapp

import android.location.Location
import androidx.lifecycle.MutableLiveData
import com.onixtest.weatherapp.arch.BaseViewModel
import com.onixtest.weatherapp.repository.local.db.entities.CoordsEntity
import com.onixtest.weatherapp.repository.models.Language
import com.onixtest.weatherapp.repository.models.LocationModel
import com.onixtest.weatherapp.repository.models.Settings
import com.onixtest.weatherapp.repository.models.openweathermodel.OpenWeather
import com.onixtest.weatherapp.utils.Params

class MainActivityViewModel : BaseViewModel() {
    val settings = MutableLiveData<Settings?>()

    val dataDailyForecast = MutableLiveData<OpenWeather>()

    val locations = MutableLiveData<List<LocationModel>>(emptyList())

    val isDBEmpty = MutableLiveData<Boolean>()

    init {
        settings.value = sharedPreferences.settings ?: DEFAULT_SETTINGS
    }

    fun updateSettings() {
        settings.value = sharedPreferences.settings
    }

    fun setLastLocation(location: Location?) {
        if (location != null) {
            db.locationsDAO().insert(CoordsEntity(location.latitude, location.longitude, isChosen = true))
        } else {
            db.locationsDAO().insert(CoordsEntity(48.30, 32.16, isChosen = true))
        }
        isDBEmpty.value = false
    }

    fun checkDb() {
        isDBEmpty.value = db.locationsDAO().getCoordsList().isEmpty()
    }

    companion object {
        val DEFAULT_SETTINGS = Settings(
            isNightMode = false, isNotificationEnabled = false, isVeryHot = false,
            isHot = false,
            isSnow = false,
            isRussianLang = Language.RUSSIAN,
            acceptedParams = listOf(
                Params.TEMP, Params.HUMID
            ),
            declinedParams = listOf(Params.WIND, Params.DATE)
        )
    }
}