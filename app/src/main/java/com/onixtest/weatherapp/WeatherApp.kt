package com.onixtest.weatherapp

import android.app.Application
import android.content.Context
import com.onixtest.weatherapp.di.appModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.GlobalContext.startKoin

class WeatherApp : Application() {

    init {
        instance = this
    }

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@WeatherApp)
            modules(appModule)
        }
    }

    companion object {
        private var instance: WeatherApp? = null
        val applicationContext: Context
            get() = instance!!.applicationContext
    }
}