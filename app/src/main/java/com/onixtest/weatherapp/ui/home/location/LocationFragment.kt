package com.onixtest.weatherapp.ui.home.location

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.google.android.gms.common.api.Status
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener
import com.google.android.material.snackbar.Snackbar
import com.onixtest.weatherapp.MainActivity
import com.onixtest.weatherapp.MainActivityViewModel
import com.onixtest.weatherapp.R
import com.onixtest.weatherapp.arch.BaseFragment
import com.onixtest.weatherapp.databinding.FragmentLocationBinding

class LocationFragment : BaseFragment<FragmentLocationBinding>(), OnMapReadyCallback {

    private val locationViewModel: LocationViewModel by viewModels()

    private val mainActivityViewModel: MainActivityViewModel by activityViewModels()

    private var chosenPlace: Place? = null

    private var mGoogleMap: GoogleMap? = null

    override fun inflateBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ) = FragmentLocationBinding.inflate(inflater, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapFragment = childFragmentManager.findFragmentById(R.id.fMap) as? SupportMapFragment
        mapFragment?.getMapAsync(this)
        setOnClickListeners()
        initObservers()
        addAutoCompleteApi()
    }

    private fun setupMap() {
        mGoogleMap?.apply {
            val currentLatLng = if (chosenPlace == null) {
                LatLng(
                    mainActivityViewModel.dataDailyForecast.value?.lat ?: 0.0,
                    mainActivityViewModel.dataDailyForecast.value?.lon ?: 0.0
                )
            } else {
                LatLng(chosenPlace?.latLng?.latitude ?: 0.0, chosenPlace?.latLng?.longitude ?: 0.0)
            }
            val zoomLevel = 6f //This goes up to 21
            addMarker(
                MarkerOptions()
                    .position(currentLatLng)
            )
            moveCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, zoomLevel))
        }
    }

    private fun initObservers() {
        locationViewModel.location.observe(viewLifecycleOwner, {
            val listLocations = mainActivityViewModel.locations.value?.toMutableList()
            listLocations?.add(0, it)
            mainActivityViewModel.locations.value = listLocations
            findNavController().navigate(LocationFragmentDirections.actionLocationFragmentToHomeFragment())
        })
        locationViewModel.errors.observe(viewLifecycleOwner, { error ->
            when (error) {
                Errors.COROUTINE_ERROR -> {
                }
                Errors.RESPONSE_ERROR -> {
                }
                Errors.COORDS_ERROR -> {
                }
                Errors.SIZE_ERROR -> {
                }
                else -> {
                }
            }
        })
    }

    private fun setOnClickListeners() {
        binding.apply {
            btnAddLocation.setOnClickListener {
                if (chosenPlace != null)
                    locationViewModel.updateLocation(
                        chosenPlace?.latLng?.latitude,
                        chosenPlace?.latLng?.longitude
                    )
            }
            btnBack.setOnClickListener {
                findNavController().navigate(LocationFragmentDirections.actionLocationFragmentToHomeFragment())
            }
        }
    }

    private fun addAutoCompleteApi() {
        Places.initialize(
            requireContext(),
            requireContext().getText(R.string.google_api_id) as String
        )
        val autocompleteFragment =
            (childFragmentManager.findFragmentById(R.id.autocomplete_fragment)
                    as AutocompleteSupportFragment?)
        autocompleteFragment?.setPlaceFields(
            listOf(
                Place.Field.ID,
                Place.Field.NAME,
                Place.Field.LAT_LNG
            )
        )
        autocompleteFragment?.setOnPlaceSelectedListener(object : PlaceSelectionListener {
            override fun onPlaceSelected(place: Place) {
                mGoogleMap?.clear()
                chosenPlace = place
                setupMap()
                binding.btnAddLocation.visibility = View.VISIBLE
            }

            override fun onError(status: Status) {
                Snackbar.make(requireView(), status.toString(), Snackbar.LENGTH_LONG).show()
            }
        })
    }

    override fun onStart() {
        super.onStart()
        (activity as MainActivity?)?.bottomMenuVisibility(true)
    }

    enum class Errors {
        SIZE_ERROR,
        COORDS_ERROR,
        RESPONSE_ERROR,
        COROUTINE_ERROR
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mGoogleMap = googleMap
        setupMap()
    }
}