package com.onixtest.weatherapp.ui.home.adapter.helper

interface ItemTouchHelperAdapter {

    fun onItemDismiss(position: Int)
}