package com.onixtest.weatherapp.ui.splash

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import com.onixtest.weatherapp.MainActivity
import com.onixtest.weatherapp.MainActivityViewModel
import com.onixtest.weatherapp.R
import com.onixtest.weatherapp.arch.BaseFragment
import com.onixtest.weatherapp.databinding.FragmentSplashBinding

class SplashFragment : BaseFragment<FragmentSplashBinding>() {

    private val splashViewModel: SplashViewModel by viewModels()

    private val mainActivityViewModel: MainActivityViewModel by activityViewModels()

    override fun inflateBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ) = FragmentSplashBinding.inflate(inflater, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initObservers()
    }

    private fun initObservers() {
        splashViewModel.dataDaily.observe(viewLifecycleOwner, {
            if (it != null) {
                mainActivityViewModel.dataDailyForecast.postValue(it)
                val navOptions = NavOptions.Builder()
                    .setPopUpTo(R.id.navigation_splash, true)
                    .build()
                findNavController().navigate(
                    SplashFragmentDirections.actionSplashFragmentToOnBoardingFragment(), navOptions
                )
            }
        })
        mainActivityViewModel.isDBEmpty.observe(viewLifecycleOwner, {
            if (it == false) splashViewModel.getData()
        })
    }

    override fun onStart() {
        super.onStart()
        (activity as MainActivity?)?.bottomMenuVisibility(false)
    }
}