package com.onixtest.weatherapp.ui.dashboard.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.onixtest.weatherapp.MainActivity
import com.onixtest.weatherapp.MainActivityViewModel
import com.onixtest.weatherapp.arch.BaseFragment
import com.onixtest.weatherapp.databinding.FragmentDashboardBinding
import com.onixtest.weatherapp.utils.Geocoder
import com.onixtest.weatherapp.utils.Params
import com.onixtest.weatherapp.utils.WeatherIconSelect
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.roundToInt

class DashboardFragment : BaseFragment<FragmentDashboardBinding>() {

    private val dashboardViewModel: DashboardViewModel by viewModels()

    private val mainActivityViewModel: MainActivityViewModel by activityViewModels()

    override fun inflateBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ) = FragmentDashboardBinding.inflate(inflater, container, false)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dashboardViewModel.getTime()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setData()
        initObserver()
        setOnClickListeners()
        setRefreshListener()
    }

    private fun setOnClickListeners() {
        binding.clContainer.setOnClickListener {
            findNavController().navigate(DashboardFragmentDirections.actionDashboardFragmentToDailyForecastFragment())
        }
    }

    override fun onStart() {
        super.onStart()
        (activity as MainActivity?)?.bottomMenuVisibility(true)
    }

    private fun setRefreshListener() {
        binding.srContainer.setOnRefreshListener {
            dashboardViewModel.getData()
        }
    }

    private fun setData() {
        binding.apply {
            tvDate.text = SimpleDateFormat("EEEE, dd.MM.yyyy", Locale.getDefault()).format(Date())
            tvTemperature.text = "--"
            tvHumidity.text = "Влажность: ---"
            tvWindSpeed.text = "Скорость ветра: --- m/c"
        }
    }

    private fun initObserver() {
        dashboardViewModel.time.observe(viewLifecycleOwner, {
            binding.tvTime.text = it
        })
        mainActivityViewModel.settings.observe(viewLifecycleOwner, {
            if (it != null) {
                binding.apply {
                    tvTemperature.isVisible = it.acceptedParams?.contains(Params.TEMP) == true
                    tvHumidity.isVisible = it.acceptedParams?.contains(Params.HUMID) == true
                    tvWindSpeed.isVisible = it.acceptedParams?.contains(Params.WIND) == true
                    tvDate.isVisible = it.acceptedParams?.contains(Params.DATE) == true
                }
            }
        })
        mainActivityViewModel.dataDailyForecast.observe(viewLifecycleOwner, {
            binding.srContainer.isRefreshing = false
            if (it != null) {
                val humidity = "Влажность: ${it.current.humidity}%"
                val windSpeed = "Скорость ветра: ${it.current.wind_speed} м/с"
                var tempSign = if (it.current.temp > 0) "+" else "-"
                tempSign += "${it.current.temp.roundToInt()}"
                val coordinates = "${it.lat}, ${it.lon}"
                binding.apply {
                    tvTemperature.text = tempSign
                    tvHumidity.text = humidity
                    tvWindSpeed.text = windSpeed
                    tvLocation.text = Geocoder.setCityName(tvLocation.context, it.lat, it.lon, true)
                    tvCoordinates.text = coordinates
                    tvWeatherImage.text = WeatherIconSelect.getWeatherIcon(
                        it.current.weather.first().id,
                        it.current.sunset,
                        it.current.sunrise,
                        requireContext()
                    )
                }
            }
        })
        dashboardViewModel.dataDaily.observe(viewLifecycleOwner, {
            mainActivityViewModel.dataDailyForecast.value = it
        })
    }

    companion object {
        const val TIMER_START_DELAY = 0L
        const val TIMER_DELAY = 1000L
    }
}