package com.onixtest.weatherapp.ui.onboarding

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import com.onixtest.weatherapp.R
import com.onixtest.weatherapp.arch.BaseFragment
import com.onixtest.weatherapp.databinding.FragmentTipsBinding
import com.onixtest.weatherapp.utils.Const.ARG_POSITION

class TipsFragment : BaseFragment<FragmentTipsBinding>() {
    private var position: Int = 0

    override fun inflateBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ) = FragmentTipsBinding.inflate(inflater, container, false)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        position = arguments?.getInt(ARG_POSITION) ?: 0
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        when (position) {
            0 -> {
                binding.apply {
                    btnNextTip.text = getText(R.string.next_tip)
                    tvTitle.text = getText(R.string.rain_title)
                    tvBody.text = getText(R.string.rain_tips)
                    tvImage.text = getText(R.string.weather_cloudy)
                    btnNextTip.setOnClickListener {
                        (requireParentFragment() as OnBoardingFragment).blockSwipes()
                        (requireParentFragment() as OnBoardingFragment).setNextScreen(position + 1)
                    }
                }
            }
            1 -> {
                binding.apply {
                    btnNextTip.text = getText(R.string.next_tip)
                    tvTitle.text = getText(R.string.cloudy_title)
                    tvBody.text = getText(R.string.cloudy_tips)
                    tvImage.text = getText(R.string.weather_cloud)
                    btnNextTip.setOnClickListener {
                        (requireParentFragment() as OnBoardingFragment).blockSwipes()
                        (requireParentFragment() as OnBoardingFragment).setNextScreen(position + 1)
                    }
                }
            }
            else -> {
                binding.apply {
                    btnNextTip.text = getText(R.string.go_to_app)
                    tvTitle.text = getText(R.string.sun_title)
                    tvBody.text = getText(R.string.sun_tips)
                    tvImage.text = getText(R.string.weather_sunny)
                    btnNextTip.setOnClickListener {
                        val navOptions = NavOptions.Builder()
                            .setPopUpTo(R.id.navigation_on_boarding, true)
                            .build()
                        findNavController().navigate(
                            OnBoardingFragmentDirections.actionOnBoardingFragmentToDashboardFragment(),
                            navOptions
                        )
                    }
                }
            }
        }
    }

    fun newInstance(position: Int): Fragment {
        val tipsFragment = TipsFragment()
        val args = Bundle()
        args.putInt(ARG_POSITION, position)
        tipsFragment.arguments = args
        return tipsFragment
    }
}