package com.onixtest.weatherapp.ui.onboarding

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.onixtest.weatherapp.MainActivity
import com.onixtest.weatherapp.arch.BaseFragment
import com.onixtest.weatherapp.databinding.FragmentOnBoardingBinding
import com.onixtest.weatherapp.ui.onboarding.adapter.OnBoardingAdapter


class OnBoardingFragment : BaseFragment<FragmentOnBoardingBinding>() {

    override fun inflateBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ) = FragmentOnBoardingBinding.inflate(inflater, container, false)

    override fun onStart() {
        super.onStart()
        (activity as MainActivity?)?.bottomMenuVisibility(false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.vpOnBoarding.adapter = OnBoardingAdapter(this)
    }

    fun blockSwipes() {
        binding.vpOnBoarding.isUserInputEnabled = false
    }

    fun setNextScreen(position: Int) {
        binding.vpOnBoarding.currentItem = position
    }
}