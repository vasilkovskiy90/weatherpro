package com.onixtest.weatherapp.ui.settings.dialog

import com.onixtest.weatherapp.arch.BaseViewModel
import com.onixtest.weatherapp.repository.models.Settings

class LanguageDialogViewModel : BaseViewModel() {

    fun saveSettings(settings: Settings) {
        sharedPreferences.settings = settings
    }
}