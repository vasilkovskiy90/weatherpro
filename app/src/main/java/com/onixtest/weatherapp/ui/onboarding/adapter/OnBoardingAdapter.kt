package com.onixtest.weatherapp.ui.onboarding.adapter

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.onixtest.weatherapp.ui.onboarding.TipsFragment

class OnBoardingAdapter(fa: Fragment): FragmentStateAdapter(fa) {

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> {
                TipsFragment().newInstance(position)
            }
            1 -> {
                TipsFragment().newInstance(position)
            }
            else -> {
                TipsFragment().newInstance(position)
            }
        }
    }

    override fun getItemCount(): Int {
        return 3
    }
}