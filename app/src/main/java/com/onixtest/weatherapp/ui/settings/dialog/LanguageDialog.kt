package com.onixtest.weatherapp.ui.settings.dialog

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import com.onixtest.weatherapp.MainActivityViewModel
import com.onixtest.weatherapp.R
import com.onixtest.weatherapp.databinding.LanguageDialogBinding
import com.onixtest.weatherapp.repository.models.Language
import com.onixtest.weatherapp.repository.models.Settings
import com.onixtest.weatherapp.utils.RoundedUtils
import com.onixtest.weatherapp.utils.setCorners
import com.onixtest.weatherapp.utils.toPx


class LanguageDialogFragment : DialogFragment() {

    var settings: Settings? = null

    private val languageDialogViewModel: LanguageDialogViewModel by viewModels()
    private val mainActivityViewModel: MainActivityViewModel by activityViewModels()

    private var _binding: LanguageDialogBinding? = null

    private val binding get() = _binding!!

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return Dialog(requireContext()).apply {
            window?.setBackgroundDrawableResource(R.color.transparent)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = LanguageDialogBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initObserver()
        bindUI()
    }

    private fun initObserver() {
        mainActivityViewModel.settings.observe(viewLifecycleOwner, {
            settings = it
        })
    }

    /**
     * Initialing views: sets onClickListener
     */
    private fun bindUI() {
        binding.apply {
            clDialogContainer.setCorners(15.toPx(), RoundedUtils.RoundedType.ALL_CORNERS)
            tvRussian.setOnClickListener {
                settings?.let {
                    languageDialogViewModel.saveSettings(
                        it.apply { isRussianLang = Language.RUSSIAN }
                    )
                }
                mainActivityViewModel.updateSettings()
                dismiss()
            }
            tvEnglish.setOnClickListener {
                settings?.let {
                    languageDialogViewModel.saveSettings(
                        it.apply { isRussianLang = Language.ENGLISH }
                    )
                }
                mainActivityViewModel.updateSettings()
                dismiss()
            }
        }
    }

    override fun onStart() {
        super.onStart()
        val dialog = dialog
        if (dialog != null) {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.WRAP_CONTENT
            dialog.window?.setLayout(width, height)
            val inset = InsetDrawable(ColorDrawable(Color.TRANSPARENT), 40)
            dialog.window?.setBackgroundDrawable(inset)
        }
    }
}