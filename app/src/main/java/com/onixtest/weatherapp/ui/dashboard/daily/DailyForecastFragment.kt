package com.onixtest.weatherapp.ui.dashboard.daily

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.onixtest.weatherapp.MainActivity
import com.onixtest.weatherapp.MainActivityViewModel
import com.onixtest.weatherapp.arch.BaseFragment
import com.onixtest.weatherapp.databinding.FragmentDailyForecastBinding
import com.onixtest.weatherapp.ui.dashboard.daily.adapter.DailyForecastAdapter

class DailyForecastFragment : BaseFragment<FragmentDailyForecastBinding>() {

    private val dailyForecastViewModel: DailyForecastViewModel by viewModels()

    private val mainActivityViewModel: MainActivityViewModel by activityViewModels()

    lateinit var forecastAdapter: DailyForecastAdapter

    override fun inflateBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentDailyForecastBinding =
        FragmentDailyForecastBinding.inflate(inflater, container, false)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        forecastAdapter = DailyForecastAdapter(object : DailyForecastAdapter.ClickListener {
            override fun onClick() {
                findNavController().navigate(DailyForecastFragmentDirections.actionDailyForecastFragmentToDashboardFragment())
            }
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setOnClickListeners()
        initObserver()
        setRecyclerView()
    }

    private fun setRecyclerView() {
        binding.rvDailyForecast.apply {
            adapter = forecastAdapter
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        }
    }

    private fun setOnClickListeners() {
        binding.clDailyForecastContainer.setOnClickListener {
            findNavController().navigate(DailyForecastFragmentDirections.actionDailyForecastFragmentToDashboardFragment())
        }
    }

    override fun onStart() {
        super.onStart()
        (activity as MainActivity?)?.bottomMenuVisibility(true)
    }

    private fun initObserver() {
        mainActivityViewModel.dataDailyForecast.observe(viewLifecycleOwner, {
            forecastAdapter.submitList(it.daily.subList(0, 5))
            val coordinates = "${it.lat}, ${it.lon}"
            binding.apply {
                tvCoordinates.text = coordinates
                tvLocation.text = it.timezone
            }
        })
    }
}