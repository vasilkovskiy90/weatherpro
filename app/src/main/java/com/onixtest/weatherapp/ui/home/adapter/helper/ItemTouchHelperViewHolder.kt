package com.onixtest.weatherapp.ui.home.adapter.helper

interface ItemTouchHelperViewHolder {
    fun onItemSelected()
    fun onItemClear()
}