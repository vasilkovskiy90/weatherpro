package com.onixtest.weatherapp.ui.dashboard.main

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.onixtest.weatherapp.arch.BaseViewModel
import com.onixtest.weatherapp.repository.models.openweathermodel.OpenWeather
import com.onixtest.weatherapp.utils.Const.API_EXCLUDE
import com.onixtest.weatherapp.utils.Const.API_KEY
import com.onixtest.weatherapp.utils.Const.API_UNIT_STRING
import java.text.SimpleDateFormat
import java.util.*
import kotlin.concurrent.scheduleAtFixedRate

class DashboardViewModel : BaseViewModel() {

    private val _dataDaily = MutableLiveData<OpenWeather>()
    val dataDaily: LiveData<OpenWeather> = _dataDaily

    private val _time = MutableLiveData<String>()
    val time: LiveData<String> = _time

    fun getData() {
        launchCoroutine({
            val response = api.getDailyWeather(
                48.3038, 32.1600,
                API_UNIT_STRING,
                API_EXCLUDE,
                API_KEY
            )
            if (response.isSuccessful) {
                _dataDaily.value = response.body()
            } else {
                Log.d("++", "sheet")
            }
        }, {
            Log.d("++", it.message.toString())
        })
    }

    fun getTime() {
        launchCoroutine({
            Timer().scheduleAtFixedRate(
                DashboardFragment.TIMER_START_DELAY,
                DashboardFragment.TIMER_DELAY
            ) {
                _time.postValue(SimpleDateFormat("HH:mm", Locale.getDefault()).format(Date()))
            }
        }, {})
    }

}

