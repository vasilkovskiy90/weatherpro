package com.onixtest.weatherapp.ui.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.onixtest.weatherapp.databinding.LocationItemBinding
import com.onixtest.weatherapp.repository.models.LocationModel
import com.onixtest.weatherapp.ui.home.adapter.helper.ItemTouchHelperAdapter
import com.onixtest.weatherapp.ui.home.adapter.helper.ItemTouchHelperViewHolder
import com.onixtest.weatherapp.utils.Geocoder
import com.onixtest.weatherapp.utils.WeatherIconSelect

class LocationsAdapter(
    private val clickListener: ItemClickListener,
    private val itemRemoveListener: ItemClickListener
) :
    ListAdapter<LocationModel, LocationsAdapter.ItemViewHolder>(DiffCallback()),
    ItemTouchHelperAdapter {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        return ItemViewHolder(
            LocationItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class ItemViewHolder(private val viewBinding: LocationItemBinding) :
        RecyclerView.ViewHolder(viewBinding.root), ItemTouchHelperViewHolder {
        fun bind(item: LocationModel) {
            viewBinding.apply {
                tvLocation.text =
                    Geocoder.setCityName(tvLocation.context, item.lat, item.long, true)
                tvTodayTemp.text = item.tempToday
                tvTomorrowTemp.text = item.tempTomorrow
                tvTodayIcon.text = WeatherIconSelect.getWeatherIcon(
                    item.todayIcon,
                    item.todaySunset,
                    item.todaySunrise,
                    viewBinding.root.context
                )
                tvTomorrowIcon.text = WeatherIconSelect.getWeatherIcon(
                    item.todayIcon,
                    item.tomorrowSunset,
                    item.tomorrowSunrise,
                    viewBinding.root.context
                )
                clContainer.setOnClickListener {
                    clickListener.onClick(item)
                }
                vwFlag.isVisible = item.isChosen
            }
        }

        override fun onItemSelected() {
            itemView.setBackgroundColor(0)
        }

        override fun onItemClear() {
            itemView.setBackgroundColor(0)
        }
    }

    interface ItemClickListener {
        fun onClick(item: LocationModel)
    }

    override fun onItemDismiss(position: Int) {
        itemRemoveListener.onClick(currentList[position])
    }
}

class DiffCallback : DiffUtil.ItemCallback<LocationModel>() {
    override fun areItemsTheSame(oldItem: LocationModel, newItem: LocationModel): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: LocationModel, newItem: LocationModel): Boolean {
        return oldItem == newItem
    }
}


