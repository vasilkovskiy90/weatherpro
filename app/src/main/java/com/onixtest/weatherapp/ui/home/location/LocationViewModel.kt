package com.onixtest.weatherapp.ui.home.location

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.onixtest.weatherapp.arch.BaseViewModel
import com.onixtest.weatherapp.repository.local.db.entities.CoordsEntity
import com.onixtest.weatherapp.repository.models.Coordinates
import com.onixtest.weatherapp.repository.models.LocationModel
import com.onixtest.weatherapp.repository.models.openweathermodel.OpenWeather
import com.onixtest.weatherapp.utils.Const
import kotlin.math.roundToInt

class LocationViewModel : BaseViewModel() {

    private val _errors = MutableLiveData<LocationFragment.Errors>()
    val errors: LiveData<LocationFragment.Errors> = _errors

    private val _location = MutableLiveData<LocationModel>()
    val location: LiveData<LocationModel> = _location

    var locationCoords:Coordinates? = null

    fun updateLocation(latitude: Double?, longitude: Double?) {
        if (latitude != null && longitude != null) {
            if (db.locationsDAO().getCoordsList().size < 5){
                locationCoords = Coordinates(lat = latitude, lon = longitude)
                db.locationsDAO().insert(CoordsEntity(latitude = latitude, longitude = longitude, false))
                getData()
            } else {
                _errors.value = LocationFragment.Errors.SIZE_ERROR
            }
        } else {
            _errors.value = LocationFragment.Errors.COORDS_ERROR
        }
    }

    private fun getData() {
        launchCoroutine({
            val response = api.getDailyWeather(
                locationCoords?.lat, locationCoords?.lon,
                Const.API_UNIT_STRING,
                Const.API_EXCLUDE,
                Const.API_KEY
            )
            if (response.isSuccessful) {
                _location.value = response.body()?.let { createLocationModel(it, db.locationsDAO().getCoordsList().last().id) }
            } else {
                _errors.value = LocationFragment.Errors.RESPONSE_ERROR
            }
        }, {
            _errors.value = LocationFragment.Errors.COROUTINE_ERROR
        })
    }

    private fun createLocationModel(body: OpenWeather, id: Int): LocationModel {
        val tempSignToday = if (body.current.temp > 0) "+" else "-"
        val tempSignTomorrow = if (body.daily.first().temp.day > 0) "+" else "-"

        return LocationModel(
            tempToday = tempSignToday + body.current.temp.roundToInt().toString(),
            tempTomorrow = tempSignTomorrow + body.daily.first().temp.day.roundToInt().toString(),
            location = body.timezone,
            lat = body.lat,
            long = body.lon,
            todaySunset = body.current.sunset,
            todaySunrise = body.current.sunrise,
            tomorrowSunset = body.daily.first().sunset,
            tomorrowSunrise = body.daily.first().sunrise,
            tomorrowIcon = body.daily.first().weather.first().id,
            todayIcon = body.current.weather.first().id,
            id = id
        )
    }

}