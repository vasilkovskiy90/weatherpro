package com.onixtest.weatherapp.ui.settings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.onixtest.weatherapp.MainActivity
import com.onixtest.weatherapp.MainActivityViewModel
import com.onixtest.weatherapp.R
import com.onixtest.weatherapp.arch.BaseFragment
import com.onixtest.weatherapp.databinding.FragmentSettingsBinding
import com.onixtest.weatherapp.repository.models.Language
import com.onixtest.weatherapp.repository.models.Settings
import com.onixtest.weatherapp.ui.settings.adapter.CustomAdapter
import com.onixtest.weatherapp.ui.settings.adapter.CustomListener
import com.onixtest.weatherapp.utils.Params
import com.onixtest.weatherapp.utils.falseIfNull
import java.util.*

class SettingsFragment : BaseFragment<FragmentSettingsBinding>(), CustomListener {

    private val settingsViewModel: SettingsViewModel by viewModels()
    private val mainActivityViewModel: MainActivityViewModel by activityViewModels()

    var settings: Settings? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getSettings()
        setOnClickListeners()
    }

    private fun setOnClickListeners() {
        binding.apply {
            sbNightMode.setOnCheckedChangeListener { _, isChecked ->
                settings?.let {
                    settingsViewModel.saveSettings(
                        it.apply { isNightMode = isChecked }
                    )
                }
                mainActivityViewModel.updateSettings()
            }
            sbShowAlerts.setOnCheckedChangeListener { _, isChecked ->
                clAlerts.isVisible = isChecked
                settings?.let {
                    settingsViewModel.saveSettings(
                        it.apply { isNotificationEnabled = isChecked }
                    )
                }
                mainActivityViewModel.updateSettings()
            }
            sbHot.setOnCheckedChangeListener { _, isChecked ->
                settings?.let {
                    settingsViewModel.saveSettings(
                        it.apply { isHot = isChecked }
                    )
                }
                mainActivityViewModel.updateSettings()
            }
            sbVeryHot.setOnCheckedChangeListener { _, isChecked ->
                settings?.let {
                    settingsViewModel.saveSettings(
                        it.apply { isVeryHot = isChecked }
                    )
                }
                mainActivityViewModel.updateSettings()
            }
            sbSnow.setOnCheckedChangeListener { _, isChecked ->
                settings?.let {
                    settingsViewModel.saveSettings(
                        it.apply { isSnow = isChecked }
                    )
                }
                mainActivityViewModel.updateSettings()
            }
            tvChosenLanguage.setOnClickListener {
                findNavController().navigate(
                    SettingsFragmentDirections.actionSettingsFragmentToLanguageDialogFragment()
                )
            }
        }
    }

    private fun getSettings() {
        mainActivityViewModel.settings.observe(viewLifecycleOwner, {
            settings = it
            binding.apply {
                rvAcceptedParams.init(it?.acceptedParams ?: listOf(Params.TEMP, Params.HUMID), true)
                rvDeclinedParams.init(it?.declinedParams ?: listOf(Params.WIND, Params.DATE), false)
                tvChosenLanguage.text =
                    if (it?.isRussianLang == Language.RUSSIAN) "Русский" else "English"
                sbNightMode.isChecked = it?.isNightMode.falseIfNull()
                sbShowAlerts.isChecked = it?.isNotificationEnabled.falseIfNull()
                sbHot.isChecked = it?.isHot.falseIfNull()
                sbVeryHot.isChecked = it?.isVeryHot.falseIfNull()
                sbSnow.isChecked = it?.isSnow.falseIfNull()
            }
        })
    }

    override fun inflateBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ) = FragmentSettingsBinding.inflate(inflater, container, false)

    override fun onStart() {
        super.onStart()
        (activity as MainActivity?)?.bottomMenuVisibility(true)
    }

    private fun RecyclerView.init(paramsList: List<Params>, flag: Boolean) {
        this.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        val adapter = CustomAdapter(
            paramsList, this@SettingsFragment,
            object : CustomAdapter.ChangeListListener {
                override fun onChangeList(list: MutableList<Params>, flag: Boolean) {
                    when (flag) {
                        true -> {
                            settings?.let {
                                settingsViewModel.saveSettings(
                                    it.apply { acceptedParams = list }
                                )
                            }
                        }
                        false -> {
                            settings?.let {
                                settingsViewModel.saveSettings(
                                    it.apply { declinedParams = list }
                                )
                            }
                        }
                    }
                    mainActivityViewModel.updateSettings()
                }
            }, flag
        )
        this.adapter = adapter
        this.setOnDragListener(adapter.dragInstance)
    }
}