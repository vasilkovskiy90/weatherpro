package com.onixtest.weatherapp.ui.settings

import com.onixtest.weatherapp.arch.BaseViewModel
import com.onixtest.weatherapp.repository.models.Settings

class SettingsViewModel : BaseViewModel() {

    fun saveSettings(settings: Settings) {
        sharedPreferences.settings = settings
    }
}