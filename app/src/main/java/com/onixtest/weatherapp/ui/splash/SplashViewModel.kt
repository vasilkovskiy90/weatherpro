package com.onixtest.weatherapp.ui.splash

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.onixtest.weatherapp.arch.BaseViewModel
import com.onixtest.weatherapp.repository.models.openweathermodel.OpenWeather
import com.onixtest.weatherapp.utils.Const

class SplashViewModel : BaseViewModel() {

    private val _dataDaily = MutableLiveData<OpenWeather>()
    val dataDaily: LiveData<OpenWeather> = _dataDaily

    fun getData() {
        launchCoroutine({
            val dbList = db.locationsDAO().getCoordsList()
            for (coords in dbList) {
                if (coords.isChosen){
                    val response = api.getDailyWeather(
                        coords.latitude, coords.longitude,
                        Const.API_UNIT_STRING,
                        Const.API_EXCLUDE,
                        Const.API_KEY
                    )
                    if (response.isSuccessful) {
                        _dataDaily.postValue(response.body())
                    } else {
                        Log.d("++", "sheet")
                    }
                }
            }
        }, {
            Log.d("++", it.message.toString())
        })
    }
}