package com.onixtest.weatherapp.ui.settings.adapter

import android.content.ClipData
import android.os.Build
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.onixtest.weatherapp.R
import com.onixtest.weatherapp.ui.settings.SettingsFragment
import com.onixtest.weatherapp.utils.*

interface CustomListener

class CustomAdapter(
    private var paramsList: List<Params>,
    private val listener: CustomListener?,
    private val listListener: ChangeListListener,
    private val flag: Boolean
) : RecyclerView.Adapter<CustomAdapter.CustomViewHolder?>(), View.OnTouchListener {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return CustomViewHolder(inflater, parent)
    }

    override fun getItemCount(): Int = paramsList.size

    fun updateList(list: MutableList<Params>) {
        this.paramsList = list
        listListener.onChangeList(list, flag)
    }

    fun getList(): MutableList<Params> = this.paramsList.toMutableList()

    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
        when (event?.action) {
            MotionEvent.ACTION_DOWN -> {
                val data = ClipData.newPlainText("", "")
                val shadowBuilder = View.DragShadowBuilder(v)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    v?.startDragAndDrop(data, shadowBuilder, v, 0)
                }
                return true
            }
        }
        return false
    }

    val dragInstance: DragListener?
        get() = if (listener != null) {
            DragListener(listener)
        } else {
            Log.e(javaClass::class.simpleName, "Listener not initialized")
            null
        }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        holder.text?.text = paramsList[position].name
        holder.constraintLayout?.tag = position
        holder.constraintLayout?.setCorners(25.toPx(), RoundedUtils.RoundedType.ALL_CORNERS)
        holder.constraintLayout?.setOnTouchListener(this)
        holder.constraintLayout?.setOnDragListener(listener?.let { DragListener(it) })
    }

    class CustomViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(inflater.inflate(R.layout.settings_item, parent, false)) {
        var text: TextView? = itemView.findViewById(R.id.tvSettingTitle)
        var constraintLayout: ConstraintLayout? = itemView.findViewById(R.id.clContainer)

    }

    interface ChangeListListener {
        fun onChangeList(list: MutableList<Params>, flag: Boolean)
    }
}