package com.onixtest.weatherapp.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import com.onixtest.weatherapp.MainActivity
import com.onixtest.weatherapp.MainActivityViewModel
import com.onixtest.weatherapp.arch.BaseFragment
import com.onixtest.weatherapp.databinding.FragmentHomeBinding
import com.onixtest.weatherapp.repository.models.LocationModel
import com.onixtest.weatherapp.ui.home.adapter.LocationsAdapter
import com.onixtest.weatherapp.ui.home.adapter.helper.SimpleItemTouchHelperCallback





class HomeFragment : BaseFragment<FragmentHomeBinding>() {

    private val homeViewModel: HomeViewModel by viewModels()

    private val mainActivityViewModel:MainActivityViewModel by activityViewModels()

    private lateinit var locationsAdapter: LocationsAdapter

    private var mItemTouchHelper: ItemTouchHelper? = null

    override fun inflateBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ) = FragmentHomeBinding.inflate(inflater, container, false)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        homeViewModel.shareData = mainActivityViewModel.locations
        locationsAdapter = LocationsAdapter(object : LocationsAdapter.ItemClickListener {
            override fun onClick(item: LocationModel) {
                homeViewModel.getChosenItem(item)
            }
        }, object : LocationsAdapter.ItemClickListener {
            override fun onClick(item: LocationModel) {
                homeViewModel.removeItem(item)
            }
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if(mainActivityViewModel.locations.value?.isEmpty() == true) homeViewModel.getData()
        binding.pbLoading.show()
        setRecyclerView()
        setOnClickListeners()
        initObservers()
    }

    private fun initObservers() {
        mainActivityViewModel.locations.observe(viewLifecycleOwner, {
            binding.pbLoading.hide()
            locationsAdapter.submitList(it)
        })
        homeViewModel.singleWeatherResponse.observe(viewLifecycleOwner, {
            mainActivityViewModel.dataDailyForecast.value = it
            findNavController().navigate(HomeFragmentDirections.actionHomeFragmentToDashboardFragment())
        })
    }

    private fun setOnClickListeners() {
        binding.btnAddLocation.setOnClickListener {
            findNavController().navigate(HomeFragmentDirections.actionHomeFragmentToLocationFragment())
        }
    }

    private fun setRecyclerView() {
        binding.rvLocations.apply {
            adapter = locationsAdapter
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            val callback: ItemTouchHelper.Callback = SimpleItemTouchHelperCallback(locationsAdapter)
            mItemTouchHelper = ItemTouchHelper(callback)
            mItemTouchHelper?.attachToRecyclerView(this)
        }

    }

    override fun onStart() {
        super.onStart()
        (activity as MainActivity?)?.bottomMenuVisibility(true)
    }
}