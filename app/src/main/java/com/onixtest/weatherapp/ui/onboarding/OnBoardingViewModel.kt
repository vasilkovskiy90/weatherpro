package com.onixtest.weatherapp.ui.onboarding

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.onixtest.weatherapp.arch.livedata.SingleLiveEvent

class OnBoardingViewModel : ViewModel() {
    private val _text = MutableLiveData<String>().apply {
        value = "This is on boarding Fragment"
    }
    val text: LiveData<String> = _text
}