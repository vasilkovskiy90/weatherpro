package com.onixtest.weatherapp.ui.dashboard.daily.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.onixtest.weatherapp.repository.models.openweathermodel.innermodels.Daily
import com.onixtest.weatherapp.databinding.DailyForecastItemBinding
import com.onixtest.weatherapp.utils.WeatherIconSelect
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.roundToInt

class DailyForecastAdapter (private val clickListener: ClickListener,):
    ListAdapter<Daily, DailyForecastAdapter.ItemViewHolder>(DiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        return ItemViewHolder(
            DailyForecastItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class ItemViewHolder(private val viewBinding: DailyForecastItemBinding) :
        RecyclerView.ViewHolder(viewBinding.root) {
        fun bind(item: Daily) {
            var date = SimpleDateFormat("EEEE", Locale.getDefault()).format(Date((item.dt)*1000))
            date = date.substring(0, 1).uppercase(Locale.getDefault()) + date.substring(1)
            var tempSign = if (item.temp.day > 0) "+" else "-"
            tempSign += "${item.temp.day.roundToInt()}"
            val humidity = "${item.humidity}%"
            val windSpeed = "${item.wind_speed} m/c"
            viewBinding.apply {
                clContainer.setOnClickListener {
                    clickListener.onClick()
                }
                tvWeatherIcon.text = WeatherIconSelect.getWeatherIcon(item.weather.first().id, item.sunset, item.sunrise, viewBinding.root.context)
                tvDayOfWeek.text = date
                tvHumidity.text = humidity
                tvTemperature.text = tempSign
                tvWindSpeed.text = windSpeed
            }
        }
    }

    interface ClickListener {
        fun onClick()
    }
}

class DiffCallback : DiffUtil.ItemCallback<Daily>() {
    override fun areItemsTheSame(oldItem: Daily, newItem: Daily): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Daily, newItem: Daily): Boolean {
        return oldItem == newItem
    }
}


