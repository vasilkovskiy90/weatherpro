package com.onixtest.weatherapp.ui.home

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.onixtest.weatherapp.arch.BaseViewModel
import com.onixtest.weatherapp.repository.local.db.entities.CoordsEntity
import com.onixtest.weatherapp.repository.models.LocationModel
import com.onixtest.weatherapp.repository.models.openweathermodel.OpenWeather
import com.onixtest.weatherapp.utils.Const
import kotlin.math.roundToInt

class HomeViewModel : BaseViewModel() {


    private val _data = MutableLiveData<List<LocationModel>>()
    val data: LiveData<List<LocationModel>> = _data

    var shareData = MutableLiveData<List<LocationModel>>()

    private val _singleWeatherResponse = MutableLiveData<OpenWeather>()
    val singleWeatherResponse: LiveData<OpenWeather> = _singleWeatherResponse

    fun getData() {
        launchCoroutine({
            val dbList = db.locationsDAO().getCoordsList()
            val locationsList = mutableListOf<LocationModel>()
            for (coords in dbList) {
                val response = api.getDailyWeather(
                    coords.latitude, coords.longitude,
                    Const.API_UNIT_STRING,
                    Const.API_EXCLUDE,
                    Const.API_KEY
                )
                if (response.isSuccessful) {
                    response.body()?.let { createLocationModel(it, coords.isChosen, coords.id) }
                        ?.let { locationsList.add(it) }
                } else {
                    Log.d("++", "sheet")
                }
            }
            shareData.postValue(locationsList)
        }, {
            Log.d("++", it.message.toString())
        })
    }

    fun getChosenItem(item: LocationModel) {
        setSelector(item)
        updateDBSelector(item.id)
        getSingleData(item)
    }

    private fun updateDBSelector(id: Int) {
        var oldChosen:CoordsEntity? = null
        var newChosen:CoordsEntity? = null
         db.locationsDAO().getCoordsList().map {
             if(it.isChosen){
                 oldChosen = it.apply { isChosen = false }
             }
             if (it.id==id){
                 newChosen = it.apply { isChosen = true }
             }
         }
        oldChosen?.let { db.locationsDAO().insert(it) }
        newChosen?.let { db.locationsDAO().insert(it) }
    }

    private fun getSingleData(item: LocationModel) {
        launchCoroutine({
            val response = api.getDailyWeather(
                item.lat, item.long,
                Const.API_UNIT_STRING,
                Const.API_EXCLUDE,
                Const.API_KEY
            )
            if (response.isSuccessful) {
                if (response.body() != null) {
                    _singleWeatherResponse.value = response.body()
                } else {
                    Log.d("++", "sheet")
                }
            }
        }, {
            Log.d("++", it.message.toString())
        })
    }

    private fun setSelector(item: LocationModel) {
        shareData.value?.map {
            it.isChosen = item == it
        }
    }

    private fun createLocationModel(body: OpenWeather, isChosen: Boolean, id: Int): LocationModel {
        val tempSignToday = if (body.current.temp > 0) "+" else "-"
        val tempSignTomorrow = if (body.daily.first().temp.day > 0) "+" else "-"

        return LocationModel(
            tempToday = tempSignToday + body.current.temp.roundToInt().toString(),
            tempTomorrow = tempSignTomorrow + body.daily.first().temp.day.roundToInt().toString(),
            location = body.timezone,
            lat = body.lat,
            long = body.lon,
            todaySunset = body.current.sunset,
            todaySunrise = body.current.sunrise,
            tomorrowSunset = body.daily.first().sunset,
            tomorrowSunrise = body.daily.first().sunrise,
            tomorrowIcon = body.daily.first().weather.first().id,
            todayIcon = body.current.weather.first().id,
            isChosen = isChosen,
            id = id
        )
    }

    fun removeItem(item: LocationModel) {
        db.locationsDAO().delete(item.id)
        val editedList = shareData.value?.toMutableList()
        editedList?.remove(item)
        shareData.value = editedList.orEmpty()
    }
}