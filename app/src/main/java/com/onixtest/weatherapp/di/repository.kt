package com.onixtest.weatherapp.di

import com.onixtest.weatherapp.repository.local.db.WeatherDataBase
import com.onixtest.weatherapp.repository.local.sharedprefences.SharedPreferencesRepository
import com.onixtest.weatherapp.repository.remote.RetrofitCreator
import org.koin.android.ext.koin.androidContext
import org.koin.core.module.Module
import org.koin.dsl.module

val repository: Module = module {
    single { RetrofitCreator.getInstance() }
    single { SharedPreferencesRepository }
    single { WeatherDataBase.getInstance(androidContext()) }
}