package com.onixtest.weatherapp.arch

import androidx.lifecycle.ViewModel
import com.onixtest.weatherapp.repository.local.db.WeatherDataBase
import com.onixtest.weatherapp.repository.local.sharedprefences.SharedPreferencesRepository
import com.onixtest.weatherapp.repository.remote.Api
import kotlinx.coroutines.*
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import kotlin.coroutines.CoroutineContext

open class BaseViewModel : ViewModel(), KoinComponent, CoroutineScope {

    val api: Api by inject()
    val sharedPreferences: SharedPreferencesRepository by inject()
    val db: WeatherDataBase by inject()
    private var job: Job = SupervisorJob()
    override val coroutineContext: CoroutineContext = Dispatchers.Main + job
    private val scope = CoroutineScope(coroutineContext)

    fun launchCoroutine(block: suspend () -> Unit, handler: (Throwable) -> Unit = {}): Job {
        val coroutineExceptionHandler = CoroutineExceptionHandler { _, throwable ->
            // Handle exception
            handler.invoke(throwable)
        }
        return scope.launch(coroutineExceptionHandler) {
            block.invoke()
        }
    }
}