package com.onixtest.weatherapp.utils

import android.view.DragEvent
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.onixtest.weatherapp.R
import com.onixtest.weatherapp.ui.settings.adapter.CustomAdapter
import com.onixtest.weatherapp.ui.settings.adapter.CustomListener

class DragListener internal constructor(private val listener: CustomListener) :
    View.OnDragListener {
    private var isDropped = false
    override fun onDrag(v: View, event: DragEvent): Boolean {
        when (event.action) {
            DragEvent.ACTION_DROP -> {
                isDropped = true
                var positionTarget = -1
                val viewSource = event.localState as View?
                val viewId = v.id
                val constraintLayoutItem = R.id.clContainer
                val rvAcceptedParams = R.id.rvAcceptedParams
                val rvDeclinedParams = R.id.rvDeclinedParams
                when (viewId) {
                    constraintLayoutItem, rvAcceptedParams, rvDeclinedParams -> {
                        val target: RecyclerView
                        when (viewId) {
                            rvAcceptedParams -> target =
                                v.rootView.findViewById<View>(rvAcceptedParams) as RecyclerView
                            rvDeclinedParams -> target =
                                v.rootView.findViewById<View>(rvDeclinedParams) as RecyclerView
                            else -> {
                                target = v.parent as RecyclerView
                                positionTarget = v.tag as Int
                            }
                        }
                        if (viewSource != null) {
                            val source = viewSource.parent as RecyclerView
                            val adapterSource = source.adapter as CustomAdapter?
                            val positionSource = viewSource.tag as Int
                            val param: Params? = adapterSource?.getList()?.get(positionSource)
                            val listSource = adapterSource?.getList()?.apply {
                                removeAt(positionSource)
                            }
                            listSource?.let { adapterSource.updateList(it) }
                            adapterSource?.notifyDataSetChanged()
                            val adapterTarget = target.adapter as CustomAdapter?
                            val customListTarget = adapterTarget?.getList()
                            if (positionTarget >= 0) {
                                param?.let { customListTarget?.add(positionTarget, it) }
                            } else {
                                param?.let { customListTarget?.add(it) }
                            }
                            customListTarget?.let { adapterTarget.updateList(it) }
                            adapterTarget?.notifyDataSetChanged()
                        }
                    }
                }
            }
        }
        if (!isDropped && event.localState != null) {
            (event.localState as View).visibility = View.VISIBLE
        }
        return true
    }
}