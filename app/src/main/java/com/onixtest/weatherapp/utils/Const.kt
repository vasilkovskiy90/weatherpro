package com.onixtest.weatherapp.utils

object Const {
    const val ARG_POSITION = "POSITION"
    const val BASE_URL = "https://api.openweathermap.org/data/2.5/"
    const val API_KEY = "655615140e31c4d851669e895a923001"
    const val API_UNIT_STRING = "metric"
    const val API_EXCLUDE = "hourly,minutely"
    const val DATABASE_NAME = "WeatherAppDB"
}