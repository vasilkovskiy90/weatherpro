package com.onixtest.weatherapp.utils

import android.content.res.Resources
import kotlin.math.roundToInt

fun Int.toPx() = (this * Resources.getSystem().displayMetrics.density).roundToInt()
fun Boolean?.falseIfNull() = this ?: false