package com.onixtest.weatherapp.utils

import android.content.Context
import com.onixtest.weatherapp.R
import java.util.*

object WeatherIconSelect {
    fun getWeatherIcon(id: Int, sunset: Long, sunrise: Long, context: Context): CharSequence {
        return when (id) {
            in 200..299 -> {
                context.getText(R.string.weather_thunder)
            }
            in 300..399 -> {
                context.getText(R.string.weather_drizzle)
            }
            in 500..599 -> {
                context.getText(R.string.weather_rainy)
            }
            in 600..699 -> {
                context.getText(R.string.weather_snowy)
            }
            in 700..799 -> {
                context.getText(R.string.weather_foggy)
            }
            801 -> {
                context.getText(R.string.weather_cloud)
            }
            in 802..810 -> {
                context.getText(R.string.weather_cloudy)
            }
            else -> {
                getPartOfDay(sunrise, sunset, context)
            }

        }
    }

    private fun getPartOfDay(sunrise: Long, sunset: Long, context: Context): CharSequence {
        val calendar = Calendar.getInstance()
        val timeMillis = calendar.timeInMillis
        return if (timeMillis in (sunrise * 1000) until sunset * 1000) context.getText(R.string.weather_sunny)
        else context.getText(R.string.weather_clear_night)
    }
}