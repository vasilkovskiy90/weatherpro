package com.onixtest.weatherapp.utils

import android.content.Context
import android.location.Address
import android.location.Geocoder
import java.util.*

object Geocoder {
    fun setCityName(context: Context, latitude: Double, longitude:Double, isUkr: Boolean) : String{
            val geocoder = Geocoder(context, Locale.getDefault())
            val addresses: List<Address> =
                geocoder.getFromLocation(latitude, longitude, 1)
            if (addresses.isEmpty()){
                return "Indefinite"
            }
        return getGeolocationName(addresses[0])

    }

    private fun getGeolocationName(address: Address): String {
        return when {
            address.locality == null && address.subAdminArea == null && address.adminArea == null -> {
                address.countryName
            }
            address.locality == null && address.subAdminArea == null -> {
               address.adminArea
            }
            address.locality == null -> {
               address.subAdminArea
            }
            else -> {
                address.locality ?: "Indefinite"
            }
        }
    }

}