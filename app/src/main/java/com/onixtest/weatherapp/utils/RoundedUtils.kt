package com.onixtest.weatherapp.utils

import android.graphics.Outline
import android.view.View
import android.view.ViewOutlineProvider

/**
 * Utility to rounding a view
 */
fun View.setCorners(cornerRadius: Int?, roundedType: RoundedUtils.RoundedType) {
    RoundedUtils.setCorners(this, cornerRadius, roundedType)
}

object RoundedUtils {

    /**
     * Creates an AlertDialog with one or two buttons
     * @param view the view that needs to be rounded
     * @param cornerRadius rounding radius in pixels
     * @param roundedType rounding type {@RoundedType}
     */
    fun setCorners(
        view: View?,
        cornerRadius: Int?,
        roundedType: RoundedType
    ) {
        if (view == null || cornerRadius == null) return
        val newOutlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View, outline: Outline) {
                val left = 0
                val top = 0
                val right = view.width
                val bottom = view.height
                when (roundedType) {
                    RoundedType.ALL_CORNERS -> outline.setRoundRect(
                        left,
                        top,
                        right,
                        bottom,
                        cornerRadius.toFloat()
                    )
                    RoundedType.TOP_CORNERS -> outline.setRoundRect(
                        left,
                        top,
                        right,
                        bottom + cornerRadius,
                        cornerRadius.toFloat()
                    )
                    RoundedType.BOTTOM_CORNERS -> outline.setRoundRect(
                        left,
                        top - cornerRadius,
                        right,
                        bottom,
                        cornerRadius.toFloat()
                    )
                    RoundedType.LEFT_CORNERS -> outline.setRoundRect(
                        left,
                        top,
                        right + cornerRadius,
                        bottom,
                        cornerRadius.toFloat()
                    )
                    RoundedType.RIGHT_CORNERS -> outline.setRoundRect(
                        left - cornerRadius,
                        top,
                        right,
                        bottom,
                        cornerRadius.toFloat()
                    )
                    RoundedType.TOP_LEFT_CORNER -> outline.setRoundRect(
                        left,
                        top,
                        right + cornerRadius,
                        bottom + cornerRadius,
                        cornerRadius.toFloat()
                    )
                    RoundedType.TOP_RIGHT_CORNER -> outline.setRoundRect(
                        left - cornerRadius,
                        top,
                        right,
                        bottom + cornerRadius,
                        cornerRadius.toFloat()
                    )
                    RoundedType.BOTTOM_LEFT_CORNER -> outline.setRoundRect(
                        left,
                        top - cornerRadius,
                        right + cornerRadius,
                        bottom,
                        cornerRadius.toFloat()
                    )
                    RoundedType.BOTTOM_RIGHT_CORNER -> outline.setRoundRect(
                        left - cornerRadius,
                        top - cornerRadius,
                        right,
                        bottom,
                        cornerRadius.toFloat()
                    )
                }
            }
        }
        view.apply {
            outlineProvider = newOutlineProvider
            clipToOutline = true
        }
    }

    enum class RoundedType {
        ALL_CORNERS,
        TOP_CORNERS,
        BOTTOM_CORNERS,
        LEFT_CORNERS,
        RIGHT_CORNERS,
        TOP_LEFT_CORNER,
        TOP_RIGHT_CORNER,
        BOTTOM_LEFT_CORNER,
        BOTTOM_RIGHT_CORNER
    }
}