package com.onixtest.weatherapp.utils

enum class Params {
    TEMP,
    HUMID,
    WIND,
    DATE
}