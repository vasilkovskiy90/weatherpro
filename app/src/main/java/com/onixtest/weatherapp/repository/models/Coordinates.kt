package com.onixtest.weatherapp.repository.models

data class Coordinates(val lat:Double, val lon:Double)
