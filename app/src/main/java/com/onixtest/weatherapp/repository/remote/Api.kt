package com.onixtest.weatherapp.repository.remote
import com.onixtest.weatherapp.repository.models.openweathermodel.OpenWeather
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Routs for retrofit
 */
interface Api {
    @GET("onecall")
    suspend fun getDailyWeather(
        @Query("lat") latitude: Double?,
        @Query("lon") longitude: Double?,
        @Query("units") units: String?,
        @Query("exclude") exclude: String?,
        @Query("appid") appid: String?
    ): Response<OpenWeather>
}