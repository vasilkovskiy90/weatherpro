package com.onixtest.weatherapp.repository.models.openweathermodel.innermodels

data class Weather(
    val id: Int
)