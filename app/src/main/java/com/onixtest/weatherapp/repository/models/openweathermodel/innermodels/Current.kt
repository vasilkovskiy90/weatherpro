package com.onixtest.weatherapp.repository.models.openweathermodel.innermodels

data class Current(
    val dt: Long,
    val temp: Double,
    val humidity: Double,
    val sunset: Long,
    val sunrise: Long,
    val wind_speed: Double,
    val weather: List<Weather>
)