package com.onixtest.weatherapp.repository.local.sharedprefences

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.onixtest.weatherapp.WeatherApp
import com.onixtest.weatherapp.repository.models.Settings

private const val SETTINGS_FILE = "com.onixtest.settingsFile"
private const val KEY_SETTINGS = "key_settings"

/**
 * The shared preferences repository
 */
object SharedPreferencesRepository {

    var settings: Settings?
        get() = Gson().fromJson(getSharedPreferences().getString(KEY_SETTINGS, "").orEmpty(),
            object : TypeToken<Settings>() {}.type)
        set(value) = with(getSharedPreferences().edit()) {
            putString(KEY_SETTINGS, Gson().toJson(value))
            apply()
        }

    private fun getSharedPreferences(): SharedPreferences {
        return WeatherApp.applicationContext.getSharedPreferences(SETTINGS_FILE, Context.MODE_PRIVATE)
    }
}