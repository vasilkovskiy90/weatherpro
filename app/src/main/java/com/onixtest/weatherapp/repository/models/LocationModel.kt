package com.onixtest.weatherapp.repository.models

data class LocationModel(
    val id:Int,
    val tempToday: String,
    val tempTomorrow: String,
    val location: String,
    val lat: Double,
    val long: Double,
    val todaySunset:Long,
    val todaySunrise:Long,
    val tomorrowSunset:Long,
    val tomorrowSunrise:Long,
    val tomorrowIcon:Int,
    val todayIcon:Int,
    var isChosen:Boolean = false
)
