package com.onixtest.weatherapp.repository.local.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class CoordsEntity(
    var latitude: Double,
    var longitude: Double,
    var isChosen:Boolean,
){
    @PrimaryKey(autoGenerate = true)
    var id:Int = 0
        get() = field
}