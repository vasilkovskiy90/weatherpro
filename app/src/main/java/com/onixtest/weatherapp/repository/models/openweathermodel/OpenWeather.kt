package com.onixtest.weatherapp.repository.models.openweathermodel

import com.onixtest.weatherapp.repository.models.openweathermodel.innermodels.Current
import com.onixtest.weatherapp.repository.models.openweathermodel.innermodels.Daily

data class OpenWeather(
    val current: Current,
    val daily: List<Daily>,
    val timezone: String,
    val lat: Double,
    val lon: Double
)

