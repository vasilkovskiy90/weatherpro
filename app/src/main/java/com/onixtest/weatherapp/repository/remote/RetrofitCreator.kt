package com.onixtest.weatherapp.repository.remote

import com.google.gson.GsonBuilder
import com.onixtest.weatherapp.utils.Const.BASE_URL
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * The retrofit creator
 */
object RetrofitCreator {
    @Volatile
    private var instance: Api? = null
    private val LOCK = Any()

    /**
     * Create retrofit
     * @return api for retrofit
     */
    fun getInstance(): Api {
        if (instance == null) {
            synchronized(LOCK) {
                val gson = GsonBuilder().create()
                instance = Retrofit.Builder()
                    .client(getOkHttpClient())
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build()
                    .create(Api::class.java)
            }
        }
        return instance as Api
    }

    /**
     * Create OkHttpClient
     * @return okHttpClient
     */
    private fun getOkHttpClient(): OkHttpClient {
        val requestInterceptor = Interceptor { chain ->
            val request = chain.request()
            return@Interceptor chain.proceed(request)
        }
        val httpLoggingInterceptor: HttpLoggingInterceptor = HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }

        val builder = OkHttpClient.Builder().apply {
            addInterceptor(requestInterceptor)
            addInterceptor(httpLoggingInterceptor)
        }

        return builder.build()
    }
}