package com.onixtest.weatherapp.repository.local.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.onixtest.weatherapp.repository.local.db.entities.CoordsEntity
import com.onixtest.weatherapp.repository.local.db.dao.LocationsDao
import com.onixtest.weatherapp.utils.Const

@Database(
    entities = [
        CoordsEntity::class],
    version = 1
)
abstract class WeatherDataBase : RoomDatabase() {

    abstract fun locationsDAO(): LocationsDao

    companion object {
        @Volatile
        private var INSTANCE: WeatherDataBase? = null
        fun getInstance(context: Context): WeatherDataBase =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
            }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                WeatherDataBase::class.java, Const.DATABASE_NAME
            )
                .allowMainThreadQueries()
                .build()
    }
}