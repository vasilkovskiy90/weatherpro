package com.onixtest.weatherapp.repository.models.openweathermodel.innermodels

data class Daily(
    val dt: Long,
    val humidity: Double,
    val wind_speed: Double,
    val sunset: Long,
    val sunrise: Long,
    val temp: Temp,
    val weather: List<Weather>
)