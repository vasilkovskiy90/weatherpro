package com.onixtest.weatherapp.repository.models.openweathermodel.innermodels

data class Temp(
    val day: Double
)