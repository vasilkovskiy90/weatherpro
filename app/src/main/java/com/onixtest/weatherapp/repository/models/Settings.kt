package com.onixtest.weatherapp.repository.models

import com.onixtest.weatherapp.utils.Params

data class Settings(
    var isNightMode: Boolean?,
    var isNotificationEnabled: Boolean?,
    var isVeryHot: Boolean?,
    var isHot: Boolean?,
    var isSnow: Boolean?,
    var isRussianLang: Language?,
    var acceptedParams: List<Params>?,
    var declinedParams: List<Params>?
)

enum class Language {
    RUSSIAN,
    ENGLISH
}