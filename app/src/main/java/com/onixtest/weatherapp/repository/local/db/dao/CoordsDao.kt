package com.onixtest.weatherapp.repository.local.db.dao

import androidx.room.*
import androidx.room.Dao
import com.onixtest.weatherapp.repository.local.db.entities.CoordsEntity


@Dao
interface LocationsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(coord: CoordsEntity)

    @Query("SELECT * from CoordsEntity")
    fun getCoordsList(): List<CoordsEntity>

    @Query("DELETE FROM CoordsEntity WHERE id = :id")
    fun delete(id: Int)
}